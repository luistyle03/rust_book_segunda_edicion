#[derive(Debug)]
struct User{
    username:String,
    email:String,
    sign_in_count:u64,
    active:bool,
}

#[derive(Debug)]
struct Color(i32,i32,i32);




fn main() {
    println!("INSTANCIANDO Y ASIGNANDO VALOR");
    let mut user1=User{
        email :String::from ("abc@gmail.com"),
        username : String::from ("luistyle"),
        active:true,
        sign_in_count:1,
    };
    println!("{:?}", user1);

    println!("MODIFICANDO VALOR");
    user1.email=String::from("def@gmail.com");
    println!("{:?}", user1);

    println!("ASIGNANDO ATRIBUTOS DE OTRO STRUCT FORMA 1");
    let user2=User{
        email :String::from ("xyz@gmail.com"),
        username : String::from ("vwx"),
        active:user1.active,
        sign_in_count:user1.sign_in_count,
    };
    println!("{:?}", user2);

    println!("ASIGNANDO ATRIBUTOS DE OTRO STRUCT FORMA 2 abreviada");
    let user2=User{
        email :String::from ("xyz@gmail.com"),
        username : String::from ("vwx"),
        ..user1
    };
    println!("{:?}", user2);

    println!("ASIGNANDO DESDE UNA FUNCION QUE DEVUELVE STRUCT");
    let user1=build_user(String::from("ghi@gmail.com"), String::from("pedrostyle"));
    println!("{:?}", user1);

    println!("STRUCT TUPLA");
    let black =Color(0,0,0);
    println!("{:?}", black);
    println!("{}", black.0);
    
}

fn build_user (email:String, username:String)->User{
    //Forma larga
    /*User {
        email: email,
        username: username,
        active: false,
        sign_in_count: 0,
    }*/

    //Forma Corta: si el nombre de la variable a asignar es igual al nombre del atributo, en vez de poner email:email se coloca solo email
    User {
        email,
        username,
        active: false,
        sign_in_count: 0,
    }
}
