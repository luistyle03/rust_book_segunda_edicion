struct Rectangulo {
    ancho: u32,
    alto: u32,
}

impl Rectangulo {
    //&mut self
    fn area(&self) -> u32 {
        self.ancho * self.alto
    }

    fn puede_contener(&self, rectangulo: &Rectangulo) -> bool {
        self.alto >= rectangulo.alto && self.ancho >= rectangulo.ancho
    }


    //Funciones asociadas
    fn cuadrado(tamano: u32) -> Rectangulo {
        Rectangulo {
            ancho: tamano,
            alto: tamano,
        }
    }

 

    //constructor
    fn Rectangulo(ancho: u32, alto:u32) -> Rectangulo {
        Rectangulo {
            ancho,
            alto,
        }
    }
}

fn main() {
    //let mut rectangulo1 = Rectangulo {
    let rectangulo1 = Rectangulo {
        ancho: 30,
        alto: 50,
    };
    let rectangulo2 = Rectangulo {
        ancho: 10,
        alto: 40,
    };
    let rectangulo3 = Rectangulo {
        ancho: 60,
        alto: 45,
    };

    let sq=Rectangulo::cuadrado(3);
    let rect=Rectangulo::Rectangulo(100, 100);

    println!("El area del rectangulo ess {}", rectangulo1.area());
    println!(
        "El rectangulo 1 puede contener a rectangulo 2: {} ",
        rectangulo1.puede_contener(&rectangulo2)
    );
    println!(
        "El rectangulo 1 puede contener a rectangulo 3: {} ",
        rectangulo1.puede_contener(&rectangulo3)
    );
    println!("El area del cuadrado es {}", sq.area());
    println!("El area del rectangulo que se instancia usando constructor es {}", rect.area());
}
