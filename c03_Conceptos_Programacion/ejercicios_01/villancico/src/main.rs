fn main() {
    let mut x = [["abc"; 11] ; 3];
    //let mut x = [i32; 11] ;

    x[1,2]="Hola"

    println!("{:#?}", x);
}



/*
El primer día después de Navidad,
Mi amor me mandó
Una perdiz en un peral.

El segundo día después de Navidad,
Mi amor me mandó
Dos tórtolas
Y una perdiz en un peral.

El tercer día después de Navidad,
Mi amor me mandó
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El cuarto día después de Navidad,
Mi amor me mandó
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El quinto día después de Navidad,
Mi amor me mandó
Cinco anillos de oro,
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El sexto día después de Navidad,
Mi amor me mandó
Seis ocas poniendo,
Cinco anillos de oro,
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El séptimo día después de Navidad,
Mi amor me mandó
Siete cisnes nadando,
Seis ocas poniendo,
Cinco anillos de oro,
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El octavo día después de Navidad,
Mi amor me mandó
Ocho criadas ordeñando,
Siete cisnes nadando,
Seis ocas poniendo,
Cinco anillos de oro,
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El noveno día después de Navidad,
Mi amor me mandó
Nueve señoras bailando,
Ocho criadas ordeñando,
Siete cisnes nadando,
Seis ocas poniendo,
Cinco anillos de oro,
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El décimo día después de Navidad,
Mi amor me mandó
Diez señores brincando,
Nueve señoras bailando,
Ocho criadas ordeñando,
Siete cisnes nadando,
Seis ocas poniendo,
Cinco anillos de oro,
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El undécimo día después de Navidad,
Mi amor me mandó
Once gaiteros tocando,
Diez señores brincando,
Nueve señoras bailando,
Ocho criadas ordeñando,
Siete cisnes nadando,
Seis ocas poniendo,
Cinco anillos de oro,
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.

El duodécimo día después de Navidad,
Mi amor me mandó
Doce tamborileros tamboreando,
Once gaiteros tocando,
Diez señores brincando,
Nueve señoras bailando,
Ocho criadas ordeñando,
Siete cisnes nadando,
Seis ocas poniendo,
Cinco anillos de oro,
Cuatro pájaros llamando,
Tres gallinas francesas,
Dos tórtolas
Y una perdiz en un peral.
*/