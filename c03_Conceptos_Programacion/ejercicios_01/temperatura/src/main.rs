use std::io;

fn main() {
    let mut opcion: u32 = 0;
    let mut numero: u32;

    println!("CONVERSION DE TEMPERATURAS");
    println!("==========================");
    println!("Menu");
    println!("(1): Convertir de Celcius a Fahrenheit");
    println!("(2): Convertir de Fahrenheit a Celcius");
    println!("(0): Salir");

    loop {
        println!("Ingrese la opcion del menu => ");
        opcion = ingrese_numero();
        if opcion == 1 {
            println!("Ingrese el numero en grados Celcius => ");
            numero = ingrese_numero();
            numero = 9 * numero / 5 + 32;
            println!("En Grados Fahrenheit es :{}", numero);
        } else if opcion == 2 {
            println!("Ingrese el numero en grados Fahrenheit => ");
            numero = ingrese_numero();
            numero = (numero - 32) * 5 / 9;            
            println!("En Grados Celcius es :{}", numero);
        } else if opcion > 2 {
            println!("Ingrese un opcion correcta");
            continue;
        } else {
            break;
        }
    }
    println!("Gracias por usar el programa de conversion");
}



fn ingrese_numero() -> u32 {
    let mut seguir_preguntando: bool = true;
    let mut num: u32 = 0;

    while seguir_preguntando {
        let mut cadena = String::new();

        io::stdin()
            .read_line(&mut cadena)
            .expect("Fallo al leer la linea");

        num = match cadena.trim().parse() {
            Ok(nume) => {
                seguir_preguntando = false;
                nume
            }
            Err(_) => {
                println!("Ingrese un dato tipo numerico");
                continue;
            }
        };
    }
    num
}
