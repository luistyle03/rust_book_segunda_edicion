use std::io;

fn main() {
    let mut numero;
    let mut anterior;
    let mut anterior_anterior;
    let mut ant;
    let mut ant_ant;

    println!("Fibonacci");
    println!("=========");

    loop {
        anterior = 1;
        anterior_anterior = 0;
        println!("Ingrese el numero de elementos => ");
        numero = ingrese_numero();
        if numero > 1 {
            println!("Imprimiendo serie");
            imprime_iniciales();
            for i in (3..numero + 1) {
                //println!("{}",i);
                ant = anterior;
                ant_ant = anterior_anterior;
                println!("{}", ant + ant_ant);
                anterior = ant + ant_ant;
                anterior_anterior = ant;
            }
        } else if numero == 1 {
            println!("Ingrese un numero mayor a 1");
        } else {
            break;
        }
    }
    println!("Gracias por usar el programa");
}

fn ingrese_numero() -> u32 {
    let mut seguir_preguntando: bool = true;
    let mut num: u32 = 0;

    while seguir_preguntando {
        let mut cadena = String::new();

        io::stdin()
            .read_line(&mut cadena)
            .expect("Fallo al leer la linea");

        num = match cadena.trim().parse() {
            Ok(nume) => {
                seguir_preguntando = false;
                nume
            }
            Err(_) => {
                println!("Ingrese un dato tipo numerico entero");
                continue;
            }
        };
    }
    num
}

fn imprime_iniciales() {
    println!("{}", 0);
    println!("{}", 1);
}
