fn main() {
    
    let x = 2.0; // f64 predeterminado
    let y: f32 = 3.0; // f32
    let w: f64=5.0;
    let z:f64=x+w;

    println!("f32 {}",y);
    println!("Numero X declarado f64 por defecto {}",x);
    println!("Numero W declarado f64 explicitamente {}",w);
    println!("Numero Z declarado f64 cuya resultado es suma de X+ W {}",z);



    // Suma
    let sum = 5 + 10;

    // Diferencia
    let difference = 95.5 - 4.3;

    // Multiplicacion
    let product = 4 * 30;

    // division
    let quotient = 56.7 / 32.2;

    // residuo
    let remainder = 43 % 5;


    println!("Suma       {}",sum);
    println!("Diferencia {}",difference);
    println!("Product    {}",product);
    println!("Division   {}",quotient);
    println!("Remainder  {}",remainder);


    let t = true;
    let f: bool = false; // with explicit type annotation
    println!("t {}",t);
    println!("f {}",f);


    //Tipo de dato caracter
    /*char se especifica con comillas simples, a diferencia de los strings, que usan comillas dobles*/
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';
    println!("c {}",c);
    println!("z {}",z);
    println!("heart_eyed_cat {}",heart_eyed_cat);


    //Tupla
    let tup: (i32, f64, u8) = (500, 6.4, 1); //Declaracion explicita de tupla
    let tup = (500, 6.4, 1); //o declaracion implicita de una tupla

    println!("Primer valor de tupla: {}", tup.0); 
    println!("Segundo valor de tupla: {}", tup.1); 
    println!("Tercer valor de tupla: {}", tup.2); 
    
    let (xx, yy, zz) = tup; //Tambien se puede asignar a variables indpendientes: DESTRUCTURACION
    println!("Primer valor de tupla asignada a una variable independiente: {}", xx); 
    println!("Segundo valor de tupla  asignada a una variable independiente: {}", yy); 
    println!("Tercer valor de tupla  asignada a una variable independiente: {}", zz); 
    
    // ARRAY
    //Todos elementos deben tener el mismo tipo de datos y el numero de elementos es fijo.xx

    let a = [1, 2, 3, 4, 5];
    println!("Primer elemento de array a: {}", a[0]);
    println!("segundo elemento de array a: {}", a[1]);

     




}
