fn main() {
    println!("Funciones");
    println!("=========");

    imprime_suma(5,7);
    imprime_suma(cinco(),7);
    imprime_suma(cinco_return(),7);
    println!("Imprimiendo valor de retorno de una funcion {}",sumar(5, 7));

    let y={
        let x=3;  //declaracion instruccion que no devuelve ningun valor
        x+1    //Expresion evalua un valor resultante (NO VA PUNTO Y COMA AL FINAL)
    };
    println!("el valor de Y es {} ",y);


}

fn imprime_suma( x:i32, y:i32 ){
    println!("Imprimiendo suma desde otro funcion {}",x+y);

}
fn sumar( x:i32, y:i32 )-> i32{
    x+y
}

fn cinco()-> i32{
    5
}

fn cinco_return()-> i32{
    return 5
}

