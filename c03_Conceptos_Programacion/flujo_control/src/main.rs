fn main() {
    println!("Control de Flujo");
    println!("================");

  
        let number = 7;
    
        if number < 5 {
            println!("El numero es menor a 5");
        } else {
            println!("El numero es mayor a 5");
        }

        if number != 5 {
            println!("El numero no es igual a 5");
        }

        if number%4==0 {
            println!("Numero divisible por 4");
        }else if  number%3==0{
            println!("Numero divisible por 3");
        }else if number%2==0{
            println!("Numero divisible por 2");
        }else{
            println!("Numero NO divisible por 4,3 y 2");
        }

        //Declaracion de un numero asignado condicionalmente
        let condition = true;
        let number = if condition {
            5
        } else {
            6
        };
    
        println!("Declaracion de un numero asignado condicionalmente: {}", number);

        println!("BUCLES");
        println!("LOOP");
        let mut i=0;
        loop{
            println!("El contador es: {} ",i);            
            if i==4{
                break;
            } 
            i=i+1;
        }

        
        println!("WHILE");
        i=0;
        while i<5{
            println!("El contador es: {} ",i);
            i=i+1;          
        }

        println!("FOR para un array");
        let a = [10, 20, 30, 40, 50];
        for v in a.iter(){
            println!("El valor de la matriz es: {}",v);
        }


        println!("FOR rango");        
        for i in (0..5){
            println!("El contador es: {} ",i);
        }

        println!("FOR rango invertido");        
        for i in (0..5).rev(){
            println!("El contador es: {} ",i);
        }





  


}
