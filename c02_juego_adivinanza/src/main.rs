use std::io;
//Usamos el modulo Rng de la biblioteca crate 'Rand' el cual ha sido especificado en cargo.toml que sera descargado en nuestro proeycto al momento de compilarlo
use rand::Rng;
use std::cmp::Ordering;


fn main() {
    println!("Juego de Adivinanzas");
    println!("====================");
    
    //let: se usa como palabra reservada para declarar una variable
    //Generamos un numero entre mayor o igual que 1 y menor que 100 (por ello se coloca 101 en la siguiente instruccion)
    let numero_aleatorio=rand::thread_rng().gen_range(1,101);
    println!("El numero aleatorio es: {}, (obviamente esto no deberia salir pero para probar el programa muestro este mensaje )",numero_aleatorio);

 //sentencia de bucle infinito para salir de el, usamos break
loop{
    println!("Ingrese el numero =>");

    //Llama a la funcion New de String (metodo estatico llamado en algunos lenguajes).
    //mut: significa que el valor de la variable 'numero' puede ser cambiado posteriormente. 
    //Por defecto una variable es inmutable osea es como una "constante" (no se puede cambiar el valor posteriormente). Si deseamos que se comporte verdaderamente como una variable tenemos que decirle que sea mutable osea agregar 'mut' antes de let
    let mut numero=String::new();

    // si no hubieramos puesto use std::io, para llamar a esta funcion tendriamos que haberlo hecho asi:  std::io::stdin().read_line(&mut numero).expect("Fallo al leer la linea");
    // &: indica que se esta pasanado a la funcion read_line un pase por referencia y no por valor
    //En caso de haber un error la funcion expect recibe un mensaje de error personalizado por nosotros
    //En caso de no colocar .expect el compilador lanzara una advertencia diciendonos que un posible error no esta siendo manejado.
    io::stdin().read_line(&mut numero).expect("Fallo al leer la linea");


    //parse de u32 convierte una cadena en u32 dando como resultado un tipo de dato io::Result que no es mas que 'enum' cuyos valores son 'Ok' o 'Err'. 
    //Si el resultado es 'ok' entonces la "funcion" 'ok' llega con el numero u32 capturado, luego como no le haremos ningun procesamiento ponemos nuevamente num(es como 'return num' en una funcion de otros lenguajes de programacion) para que retorne ese valor y lo asigne a la variable numero
    //Si el resultado es 'Err' entonces la "funcion" 'Err' llega con el mensaje de error capturado, luego imprimimos en consola un mensaje de error menos tecnico añadiendo si deseamos el error que nos reporta Rust. 'Continue' hace que todo el codigo que falta ejecutar para que acabe esa iteraccion NO lo ejecute, es decir pasara inmediatamente a la siguiente iteraccion, osea ejecutara la primera instruccion del loop: 'println!("Ingrese el numero =>");'
    //Sombrear SHADOW: La variable numero se vuelve a redeclarar. Esto se usa generalmente cuado se convierte un tipo de dato de una variable . En nuestro caso en vez de haber creado numero_texto y luego otra variable numero_u32, mejor creamos numero una variable como numero de tipo string y luego volvemos a redeclarar la misma variable como tipo u32 a esto se llama sombrear (shadow)
    //trim: elimina espacios al inicio y al final de un string
    //parse: convierte un texto en tipo de dato numero
    let numero: u32 = match numero.trim().parse(){
        Ok(num) => num,
        Err(e) => {        
            //si poniamos Err(_)significa que cuando haya un error y entremos a este escenario, el mensaje de error no nos interesa recibirlo por eso ponemos el guion bajo. Simplemente sabemos que hubo un error pero no deseamos saber en que fallo.
            println!("Ingrese un dato tipo numerico entero. {}",e);
            continue; //reinicia nuevamente la iteracion
        }
    };

    //imprime en consola el valor de la variable numero. Dicho valor sera impreso en la posicion que estan las llaves {}. Ejemplo: Tu numero es {5}  (Las llaves no se imprimiran)
    println!("Tu numero: {}", numero);

    //cmp da como resultado un enum Ordering cuyas variantes son Less, Greater y Equal.
    //El match es como un swithc en otros lenguajes de programacion. Cada uno de los escenario lo llaman arms o brazos
    match numero.cmp(&numero_aleatorio){
        Ordering::Less=> println!("Es menor"),
        Ordering::Greater=> println!("Es mayor"),
        Ordering::Equal=> {
            println!("Es igual, ADIVINASTE");
            break; //sale del bucle
        }

    }
}
    


}


